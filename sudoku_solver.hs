import Data.List.Unique (sortUniq)

exampleSudoku :: [Int]
exampleSudoku = [ 0, 7, 0,  0, 2, 0,  0, 4, 6,
                  0, 6, 0,  0, 0, 0,  8, 9, 0, 
                  2, 0, 0,  8, 0, 0,  7, 1, 5, 

                  0, 8, 4,  0, 9, 7,  0, 0, 0, 
                  7, 1, 0,  0, 0, 0,  0, 5, 9, 
                  0, 0, 0,  1, 3, 0,  4, 8, 0, 

                  6, 9, 7,  0, 0, 2,  0, 0, 8, 
                  0, 5, 8,  0, 0, 0,  0, 6, 0, 
                  4, 3, 0,  0, 8, 0,  0, 7, 0 ] 

finishedSudoku :: [Int]
finishedSudoku = [ 5, 8, 6,  3, 7, 4,  9, 1, 2,
                   1, 3, 7,  9, 5, 2,  8, 6, 4,
                   2, 4, 9,  8, 1, 6,  5, 7, 3,

                   8, 7, 2,  5, 4, 3,  1, 9, 6,
                   6, 9, 3,  7, 8, 1,  2, 4, 5,
                   4, 1, 5,  6, 2, 9,  7, 3, 8,

                   9, 5, 4,  2, 3, 7,  6, 8, 1,
                   7, 2, 1,  4, 6, 8,  3, 5, 9,
                   3, 6, 8,  1, 9, 5,  4, 2, 7 ]

getRow :: Int -> [Int] -> [Int]
getRow nr = drop (9*(nr-1)) . take (9*nr)

getCol :: Int -> [Int] -> [Int]
getCol nr board | nr <= 81 = board !! (nr-1) : getCol (9+nr) board
                | otherwise = []

getBox' :: Int -> [Int] -> Int -> [Int]
getBox' nr board count | count > 0 = (take 3 (drop ((nr-1)*3) board)) ++ (getBox' nr (drop 9 board) (count - 1))
                       | count == 0 = []

getBox :: Int -> [Int] -> [Int]
getBox nr board = getBox' nr (drop ((2*((nr-1) `div` 3))*9) board) 3


getIndexCoord :: Int -> Int -> Int
getIndexCoord x y = (x-1) + ((y-1)*9)

replaceElemIndexByNum :: [Int] -> Int -> Int -> [Int]
replaceElemIndexByNum board ind num = let (xs,_:ys) = splitAt ind board
                                      in xs ++ [num] ++ ys

findFirstZero' :: [Int] -> Int -> (Maybe Int)
findFirstZero' [] _ = Nothing
findFirstZero' (x:xs) index  | x == 0 = Just index
                             | otherwise = findFirstZero' (xs) (1+index)

findFirstZero :: [Int] -> (Maybe Int)
findFirstZero board  = findFirstZero' board 0

replaceFirstZeroByNum :: [Int] -> Int -> [Int] 
replaceFirstZeroByNum board num = case (findFirstZero board) of
                                    Just x -> replaceElemIndexByNum board x num
                                    Nothing -> board

checkUnit :: [Int] -> Bool
checkUnit u = (length(sortUniq u) == 9) && (sum u == sum[1..9])

checkRows :: [Int] -> [Bool]
checkRows board = map checkUnit $ getRow <$> [1..9] <*> pure board

checkCols :: [Int] -> [Bool]
checkCols board = map checkUnit $ getCol <$> [1..9] <*> pure board

checkWins :: [Int] -> [Bool]
checkWins board = map checkUnit $ getBox <$> [1..9] <*> pure board

isSolved :: [Int] -> Bool
isSolved board = all (==True) $ concat $ [checkRows, checkCols, checkWins] <*> pure board

removeZeros :: [Int] -> [Int]
removeZeros [] = []
removeZeros (x:xs) | x == 0 = removeZeros xs
                   | otherwise = x:(removeZeros xs)

isValid :: [Int] -> Bool
isValid u = (length (removeZeros u)) == (length (sortUniq(removeZeros u))) && (sum u <= sum[1..9])

validateRows :: [Int] -> [Bool]
validateRows board = map isValid $ getRow <$> [1..9] <*> pure board

validateCols :: [Int] -> [Bool]
validateCols board = map isValid $ getCol <$> [1..9] <*> pure board

validateWins :: [Int] -> [Bool]
validateWins board = map isValid $ getBox <$> [1..9] <*> pure board

checkIfStillValid :: [Int] -> Bool
checkIfStillValid board = all (==True) $ concat $ [validateRows, validateCols, validateWins] <*> pure board

applyChoices :: [[Int]] -> [[Int]]
applyChoices boards | ok /= [] = ok
                    | otherwise = applyChoices $ filter checkIfStillValid $ concat $ map (\x -> map (replaceFirstZeroByNum x) [1..9]) boards
                      where ok = filter (isSolved) boards

solveBoard :: [Int] -> [Int]
solveBoard board = concat $ applyChoices [board]
